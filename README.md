# TrainingMangement

As a part of the project, a system should be developed to streamline the management of training


 Komanda: 


  Modestas (pilot) 
  
  Ramunas (wingman)
  
  Nerijus (BATMAN) 
  
* Rasa 
  
* Eligijus 
  
  
  Projekto tech: Spring5, SpringBoot2 + Angular 
  
  Uzdavinys: Training management system 
  
  
  Business need: n/a 
  
  
  Business Object Logic: 
   
  
  ROLES: 
  Administrator 
  Trainer 
  Student 
 
  
  SCHEDULING:
  Calendar 
   
  EMAIL/MQ/SMS:
  Notification 
    
  
    ENTITIES: 
    --------------
    
    * USER 
    
    * REGISTRATION 
    
    * COURSE (CRUD)
    
    * SUBJECT / "Class" 
    
    * NOTIFICATION 
    
    TBC...
    
    
    
