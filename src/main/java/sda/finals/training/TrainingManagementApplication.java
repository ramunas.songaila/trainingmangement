package sda.finals.training;

import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.apache.logging.log4j.LogManager;



@SpringBootApplication
public class TrainingManagementApplication {

	private static Logger log = LogManager.getLogger(TrainingManagementApplication.class);

	public static void main(String[] args) {

		log.debug( "Starting application class {}", TrainingManagementApplication.class );
		SpringApplication.run( TrainingManagementApplication.class, args );

		log.error( "Stopping application class {}", TrainingManagementApplication.class );
	}

}
