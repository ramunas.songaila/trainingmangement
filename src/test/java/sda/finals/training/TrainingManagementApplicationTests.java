package sda.finals.training;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TrainingManagementApplicationTests {

	private static Logger log = LogManager.getLogger(TrainingManagementApplicationTests.class);


	@Test
	void contextLoads() {

		log.debug( "Starting TEST application class {}", TrainingManagementApplicationTests.class );
	}

}
